<%-- 
    Document   : index
    Created on : 22-mar-2016, 10.35.49
    Author     : anatoli
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>TableMaker 9000</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form method="POST" action="<%= request.getContextPath() %>/CreaTabella">
            <p>Numero Colonne
                <input type="number" name="col" required />
            </p>
            <p>Numero Righe
                <input type="number" name="row" required />
            </p>
            <p>
                <input type="submit" />
                <input type="reset" />
            </p>
        </form>
    </body>
</html>
