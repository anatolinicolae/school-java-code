/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CreaTabella extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            try {
                int col = Integer.parseInt(request.getParameter("col"));
                int row = Integer.parseInt(request.getParameter("row"));
            
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>TableMaker 9000</title>");            
                out.println("</head>");
                out.println("<body>");
                out.println("<form method=\"POST\" action=\""+request.getContextPath()+"/CreaTabella\">");
                out.println("<p>Numero Colonne");
                out.println("<input type=\"number\" name=\"col\" placeholder=\""+col+"\" required />");
                out.println("</p>");
                out.println("<p>Numero Righe");
                out.println("<input type=\"number\" name=\"row\" placeholder=\""+row+"\" required />");
                out.println("</p>");
                out.println("<p>");
                out.println("<input type=\"submit\" />");
                out.println("<input type=\"reset\" />");
                out.println("</p>");
                out.println("</form>");
                
                if ( col > 0 && row > 0 ) {
                    out.println("<table border=\"1\" cellpadding=\"5\" cellspacing=\"0\">");
                        out.println("<tbody>");
                            for (int r = 1; r <= row; r++) {
                                out.println("<tr>");
                                for (int c = 1; c <= col; c++) {
                                    out.println("<td>"+(r*c)+"</td>");
                                }
                                out.println("</tr>");
                            }
                        out.println("</tbody>");
                    out.println("</table>");
                }
            } catch (Exception e) {
                
            }
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
