<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Color Switch</title>
    </head>
    <body>
        <h1>Color Switch</h1>
        <form action="ColorMan.c" method="POST">
            <select name="color">
                <option value="blue">Blue</option>
                <option value="green">Green</option>
                <option value="yellow">Yellow</option>
                <option value="red">Red</option>
            </select>
            <input type="submit" />
        </form>
    </body>
</html>
