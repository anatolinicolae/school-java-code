/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author anatoli
 */
public class CookieMan extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String c_name = request.getParameter("c_name");
            String c_val = request.getParameter("c_val");
            String show = request.getParameter("show");
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CookieMan</title>");            
            out.println("</head>");
            out.println("<body>");

            if ( c_name != null && c_val != null ) {
                Cookie c = new Cookie(c_name, c_val);
                response.addCookie(c);
                out.println("<h2>CookieMan ha impostato il cookie.</h2>");
                out.println("<a href=\"" + request.getContextPath() + "/CookieMan?show=cookies\">Visualizza Cookie</a>");
            }
            
            if ( show != null && show.equals("cookies") ) {
                Cookie[] cookies = request.getCookies();
                if ( cookies != null && cookies.length > 0 ) {
                    out.println("<table border=\"1\" cellpadding=\"5\" cellspacing=\"0\">");
                    out.println("<thead><tr><td>Nome</td><td>Valore</td></tr></thead>");
                    for (int i = 0; i < cookies.length; i++) {
                        Cookie cookie = cookies[i];
                        out.println("<tr><td>" + cookie.getName() + "</td><td>" + cookie.getValue() + "</td></tr>");
                    }
                    out.println("</table>");
                } else {
                    out.println("<p>Non ci sono cookie impostati.</p>");
                }
            }
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
