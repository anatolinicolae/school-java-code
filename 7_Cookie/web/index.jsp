<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CookieMan</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h2>CookieMan</h2>
        <form method="POST" action="<%= request.getContextPath() %>/CookieMan">
            <p>Nome Cookie
                <input type="text" name="c_name" required />
            </p>
            <p>Valore Cookie
                <input type="text" name="c_val" required />
            </p>
            <p>
                <input type="submit" />
                <input type="reset" />
            </p>
        </form>
    </body>
</html>
