<%@page import="java.sql.*" %>
<%@page import="sqrd.DB" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agenda</title>
    </head>
    <body>
        <h1>Agenda</h1>
        <a href="new.jsp">Aggiungi contatto</a>
        <%
            DB db = new DB();
            String sql = "SELECT * FROM contatti";
            ResultSet rs = db.query(sql);
            if ( rs.isBeforeFirst() ) {
                out.println("<table><thead><td>Nome</td><td>Email</td><td>Telefono</td></thead>");
                while ( rs.next() ) {
                    out.println("<tr><td>"+rs.getString("nome")+"</td><td>"+rs.getString("email")+"</td><td>"+rs.getString("telefono")+"</td></tr>");
                }
                out.println("</table>");
            } else {
                out.print("Nessun contatto presente.");
            }
        %>
    </body>
</html>
