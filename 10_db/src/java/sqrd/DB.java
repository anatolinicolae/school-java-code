package sqrd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class DB {
  private Connection connection = null;
  private ResultSet rs;
  private String	path = "jdbc:mysql://127.0.0.1:3306/sqrd_agenda",
                  user = "root",
                  pass = "root";

  public DB() throws Exception {
    Class.forName("com.mysql.jdbc.Driver");
    connection = (Connection) DriverManager.getConnection(path, user, pass);
  }
  
  public ResultSet query(String sql) throws Exception {
    Statement s = connection.createStatement();
    return s.executeQuery(sql);
  }
  
  public ResultSet query(String sql, String[] params) throws Exception {
    PreparedStatement s = connection.prepareStatement(sql);
    
    for (int i = 0; i < params.length; i++) {
      s.setString(i + 1, params[i]);
    }
    
    return s.executeQuery();
  }
  
  public int execute(String sql) throws Exception {
    Statement s = connection.createStatement();
    return s.executeUpdate(sql);
  }
  
  public int execute(String sql, String[] params) throws Exception {
    PreparedStatement s = connection.prepareStatement(sql);
    
    for (int i = 0; i < params.length; i++) {
      s.setString(i + 1, params[i]);
    }
    
    return s.executeUpdate();
  }
}
