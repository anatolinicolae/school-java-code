<%
    HttpSession s = request.getSession();
    Integer counter = (Integer) s.getAttribute("counter");
    if (counter == null) {
        counter = 1;
        s.setAttribute("first_time", new java.util.Date(session.getCreationTime()));
    } else {
        counter = counter.intValue() + 1;
    }
    s.setAttribute("counter", counter);
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Countatore</h1>
        <p>Visita #<%= counter %></p>
        <p>La servlet è stata creata  il <%= s.getAttribute("first_time") %></p>
        <a href="javascript:location.reload();">Ricarica</a>
    </body>
</html>
